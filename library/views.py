
import requests
from django.shortcuts import render, get_object_or_404, render_to_response
from django.urls import reverse, reverse_lazy
from library.models import Book, Author
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin

def index(request):
    books = Book.objects.all()
    authors = Author.objects.all()
    context  = {
        'books' : books,
        'authors': authors
    }
    return render(request, 'library/index.html', context)


def about(request):
    return render(request,'library/about.html')


def showBook(request, book_id):
    book = get_object_or_404(Book,pk= book_id)
    context = {
        'book': book
    }
    return render(request, 'library/showBook.html', context)

def showAuthor(request, author_id):
    author = get_object_or_404(Author,pk= author_id)
    context = {
        'author': author,
    }
    return render(request, 'library/showAuthor.html', context)

class AuthorCreate(LoginRequiredMixin,CreateView):
    model = Author
    fields = ['name']
    login_url = 'login'



class AuthorUpdate(LoginRequiredMixin,UpdateView):
    model = Author
    fields = ['name']
    login_url = 'login'


class AuthorDelete(LoginRequiredMixin,DeleteView):
    model = Author
    success_url = reverse_lazy('index')
    login_url = 'login'

class BookCreate(LoginRequiredMixin,CreateView):
    model = Book
    fields = ['title','author']
    login_url = 'login'

class BookUpdate(LoginRequiredMixin,UpdateView):
    model = Book
    fields = ['title','author']
    login_url = 'login'

class BookDelete(LoginRequiredMixin,DeleteView):
    model = Book
    success_url = reverse_lazy('index')
    login_url = 'login'

from django.db.models import Q

def search(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        books = Book.objects.filter(Q(title__icontains=q)|
                                    Q(author__name__icontains=q))
        return render_to_response('library/search.html',
                                  {'books': books, 'query': q})
    else:
        return HttpResponse('Please submit a search term.')

class RegisterForm(FormView):
    form_class = UserCreationForm
    success_url = "/login/"
    template_name = "library/register.html"
    def form_valid(self, form):
        form.save()
        return super(RegisterForm, self).form_valid(form)

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login,logout

class LoginForm(FormView):
    form_class = AuthenticationForm
    template_name = "library/login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginForm, self).form_valid(form)

from django.http import HttpResponseRedirect, request, HttpResponse
from django.views.generic.base import View

class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")