from django.conf.urls import url
import library
from library import views
from library.views import AuthorCreate, AuthorUpdate, AuthorDelete, BookCreate, BookDelete, BookUpdate

urlpatterns = [
    url(r'^$', library.views.index, name='index'),
    url(r'^index/$', library.views.index, name='index'),
    url(r'^register/$', views.RegisterForm.as_view(),name='register'),
    url(r'^login/$', views.LoginForm.as_view(), name='login'),
    url(r'^logout/$', views.Logout.as_view(),name='logout'),
    url(r'^about/$', library.views.about, name='about'),
    url(r'^search/$', library.views.search, name='search'),
    url(r'^showBook/(?P<book_id>[0-9]+)/$',library.views.showBook,name='showBook'),
    url(r'^showAuthor/(?P<author_id>[0-9]+)/$', library.views.showAuthor, name='showAuthor'),
    url(r'author/add/$', AuthorCreate.as_view(), name='author_add'),
    url(r'author/(?P<pk>\d+)/$', AuthorUpdate.as_view(), name='author_update'),
    url(r'author/(?P<pk>\d+)/delete/$', AuthorDelete.as_view(), name='author_delete'),
    url(r'book/add/$', BookCreate.as_view(), name='book_add'),
    url(r'book/(?P<pk>\d+)/$', BookUpdate.as_view(), name='book_update'),
    url(r'book/(?P<pk>\d+)/delete/$', BookDelete.as_view(), name='book_delete'),
]