from django.db import models
from django.urls import reverse


class Author(models.Model):
    name = models.CharField(max_length=140)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('showAuthor', args=[self.pk])

class Book(models.Model):
    title = models.CharField(max_length=140)
    author = models.ForeignKey(Author,models.SET_NULL, null=True, blank=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('showBook', args=[self.pk])