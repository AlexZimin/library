from django import forms
from .models import Book, Author

class FormBook(forms.ModelForm):
    class Meta:
        model = Book
        fields = ('title','author')

class FormAuthor(forms.ModelForm):
    class Meta:
        model = Author
        fields = ('name',)